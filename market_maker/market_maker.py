from __future__ import absolute_import
from time import sleep
import sys
from datetime import datetime
from os.path import getmtime
import random
import requests
import atexit
import signal
from market_maker import bitmex
from market_maker.settings import settings
from market_maker.utils import log, constants, errors

# Used for reloading the bot - saves modified times of key files
import os

UPDATE_DB_IN_SECS = 10

logger = log.setup_custom_logger('root')

class Timer:
    p_lasttime = datetime.now()

    @classmethod
    def seconds_elapsed(cls):
        return (datetime.now() - cls.p_lasttime).seconds

    @classmethod
    def reset(cls):
        cls.p_lasttime = datetime.now()

class ExchangeInterface:
    def __init__(self, dry_run=False, orderbook_type='orderBookL2'):
        self.dry_run = dry_run
        self.orderbook_type = orderbook_type
        if len(sys.argv) > 1:
            self.symbol = sys.argv[1]
        else:
            self.symbol = settings.SYMBOL

        self.bitmex = bitmex.BitMEX(base_url=settings.BASE_URL, symbol=self.symbol, login=settings.LOGIN,
                                    password=settings.PASSWORD, otpToken=settings.OTPTOKEN, apiKey=settings.API_KEY,
                                    apiSecret=settings.API_SECRET, orderIDPrefix=settings.ORDERID_PREFIX,
                                    orderbook_type=orderbook_type)

    def get_instrument(self, symbol=None):
        if symbol is None:
            symbol = self.symbol
        return self.bitmex.instrument(symbol)

    def get_recent_trades(self):
        return self.bitmex.recent_trades()

    def is_open(self):
        """Check that websockets are still open."""
        return not self.bitmex.ws.exited

    def get_orderbook(self):
        return self.bitmex.ws.get_orderbook(self.orderbook_type)

    def flush_trades(self):
        return self.bitmex.ws.flush_trades()

def check_connection(exchange):
    """Ensure the WS connections are still open."""
    return exchange.is_open()

def exit_func(exchange):
    def call():
        logger.info("Shutting down.")
        try:
            exchange.bitmex.exit()
        except errors.AuthenticationError as e:
            logger.info("Was not authenticated; Exit failed.")
        except Exception as e:
            logger.info("Exit failed: %s" % e)

        sys.exit()

    return call

def run_loop(exchange):
    while True:
        try:
            sys.stdout.flush()

            # This will restart on very short downtime, but if it's longer,
            # the MM will crash entirely as it is unable to connect to the WS on boot.
            if not check_connection(exchange):
                logger.error("Realtime data connection unexpectedly closed, restarting.")
                sys.exit(1)

            if Timer.seconds_elapsed() > UPDATE_DB_IN_SECS:
                orderbook = exchange.get_orderbook()

                if orderbook:
                    orderbook['trades'] = exchange.flush_trades()
                    orderbook['exchange'] = 'bitmex'
                    res = requests.post('http://localhost:3002/orderbook', json={
                        'symbol': orderbook['symbol'],
                        'snapshot': orderbook
                    })

                    print(res.text)

                    Timer.reset()
                # for i in res:
                #     print('{0} - {1}'.format(i['price'], i['size']))


        except Exception as e:
            # Closes websocket
            del exchange
            raise(e)

def run():
    exchange = ExchangeInterface(settings.DRY_RUN, orderbook_type='orderBook10')
    exit = exit_func(exchange)
    # Once exchange is created, register exit handler that will always be called
    # on any error.
    atexit.register(exit)

    signal.signal(signal.SIGTERM, exit)
    logger.info("Using symbol %s." % exchange.symbol)
    logger.info('BitMEX Market Maker Version: %s\n' % constants.VERSION)

    # Try/except just keeps ctrl-c from printing an ugly stacktrace
    try:
        run_loop(exchange)
    except (KeyboardInterrupt, SystemExit):
        sys.exit()
